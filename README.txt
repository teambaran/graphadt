README file for PA4

Name: Kaile Baran
ID: kbaran
Course: cmps101

FindPath.c : contains main method for pa4 and does all operations as required by pa4 assignment description.
Graph.c : Graph ADT
Graph.h : Header file for the Graph.c Graph ADT
GraphTest.c : Tests Graph ADT in isolation
List.c : List ADT
List.h : Header file for List ADT
Makefile.mak : Makefile for pa4
README.txt : this file.