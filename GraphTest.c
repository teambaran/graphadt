/****************************************************************************************
/**
*	GraphTest.c : Contains main for pa4. Performs tests on Graph ADT in isolation using a
				  user specified output file to verify proper output format.
*	Name: Kaile Baran
*	Student ID: kbaran
*	Assignment: Programming Assignment 4
*	Course: CMPS 101
*
*
*****************************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include"Graph.h"

int main(int argc, char * argv[])
{
	FILE *out;
	out = fopen(argv[1], "w");
	
	Graph G = newGraph(6);
	fprintf(out , "Disconnected graph with 6 vertices\n");	
	printGraph(out , G);
	
	
	addEdge(G , 1 , 2);
	addEdge(G , 1 , 4);
	addEdge(G , 2 , 5);
	addEdge(G , 2 , 4);
	addEdge(G , 2 , 3);
	addEdge(G , 3 , 5);
	addEdge(G , 3 , 6);
	addEdge(G , 4 , 5);
	addEdge(G , 5 , 6);
	fprintf(out , "\nConnected graph with 6 vertices\n");	
	printGraph(out , G);
	BFS(G , 4);
	
	List L = newList();
	
	getPath(L , G , 3);
	
	printList(out , L);
	fprintf(out , "%s\n");
	
	Graph H = newGraph(5);
	
	addArc(H , 1 , 4);
	addArc(H , 4 , 2);
	addArc(H , 2 , 5);
	addArc(H , 2 , 3);
	addArc(H , 3 , 5);
	fprintf(out , "\nDigraph with 5 vertices\n");	
	printGraph(out, H);
	BFS(H , 1);
	List M = newList();
	
	getPath(M , H , 3);
	fprintf(out , "\nPath from 1 to 3\n");
	printList(out , M);
	fprintf(out , "\nPath from 1 to 3 has a length of: %d\n", getDist(H , 3));
	fprintf(out , "\nParent of 4 is: %d\n", getParent(H , 4));
	
	freeGraph(&G);
	freeGraph(&H);
	freeList(&L);
	freeList(&M);
	fclose(out);
	
	
	return 0;
}